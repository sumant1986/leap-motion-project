#include <stdlib.h>
#include <glut.h>
#include <gl/GL.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <glm/glm.hpp>
#include "Leap.h"

using namespace Leap;
using namespace std;

//GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };
GLfloat m_LightPos[] = { 1.0, 1.0, 1.0, 0.0 };
//GLfloat m_LightPos[] = { 8.0f,-16.0f, 8.0f, 0.0f };
GLfloat white_light[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat specular_light[] = { 0.2, 0.2, 0.2, 1.0 };
//GLfloat lmodel_ambient[] = { 1.0, 0.1, 0.1, 1.0 };
GLfloat m_AmbientLight[] = { 1.0, 0.2, 0.2, 1.0 };
//GLfloat m_AmbientLight[] = { 0.5f, 0.5f, 0.5f, 1.0f };

int Button_index = -1;
bool isButtonPressed = false;
float lastPosX_right = 0.0f;
float lastPosY_right = 0.0f;
float lastPosZ_right = 0.0f;
float x_right;
float y_right;
float z_right;

float lastPosX_left = 0.0f;
float lastPosY_left = 0.0f;
float lastPosZ_left = 0.0f;
float x_left;
float y_left;
float z_left;

float x;
float y;
float z;


struct Vertex
{
	float xValue;
	float yValue;
	float zValue;

	float normal_x;
	float normal_y;
	float normal_z;

	int vert_id;
};

struct Face
{
	vector<int> vertexList;

	int faceID;
};

vector<Vertex> Vertex_t;
vector<Face> Face_t;
bool isFileDone = false;


float rotArray[16] = { 0.0f };
Leap::Matrix rotationMatrix;

float cLookAt[] = { 0.0f, 0.0f, 0.0f };
float cPos[] = { 5.0f, 3.0f, 5.0f };
//float cPos[] = { 0.0f, 0.0f, 0.0f };
float cUpVec[] = { 0.0f, 1.0f, 0.0f }; 

float scaleFactor = 1.0f;

void findScaleFactor();

float* crossProduct(float* vector1, float* vector2);

Controller controller;
Frame previousFrame;
Frame currentFrame;
Frame testFrame;
HandList handsInFrame;
Vector linearHandMovement;
void mouseMotion(float x, float y, float z);
int flag = 0;
int counter = 0;
void DrawGrid();
void DrawAxis();

class SampleListener : public Listener {
    public:
    virtual void onFrame(const Controller&);
};

void SampleListener::onFrame(const Controller& controller) {
	testFrame = controller.frame();

	static int count = 0;
	static float grabstrength = 0.0f;

	if((testFrame.hands().count() > 0))
	{
		handsInFrame = testFrame.hands();

		if ((testFrame.hands().count() == 2))
		{
			if (handsInFrame.rightmost().grabStrength() != 1)
			{
				if (handsInFrame.leftmost().grabStrength() == 1)
				{
					//cout << "hi11" << endl;
					if (flag == 0)
					{
						//lastPosX_right = handsInFrame[0].palmPosition().x;
						//lastPosY_right = handsInFrame[0].palmPosition().y;
						//lastPosZ_right = handsInFrame[0].palmPosition().z;
						previousFrame = testFrame;
						grabstrength = 0.0f;
						flag = 1;
					}
					else if (count < 5)
					{
						grabstrength += handsInFrame.rightmost().grabStrength();
						count++;
					}
					else if (grabstrength / 5 > 0.1)
					{
						count = 0;

						//cout << "hi1";

						Leap::Hand rightHand = handsInFrame.rightmost();

						Leap::Matrix rotationMatrix = rightHand.rotationMatrix(previousFrame);
						float rotArray[16] = { 0.0f };
						rotationMatrix.toArray4x4(rotArray);

						glm::vec3 zAxis(cPos[0], cPos[1], cPos[2]);
						glm::vec3 xAxis = glm::cross(glm::normalize(glm::vec3(cUpVec[0], cUpVec[1], cUpVec[2])), glm::normalize(zAxis));
						glm::vec3 yAxis = glm::cross(zAxis, xAxis);

						glm::mat4 camMatrix;
						camMatrix[0] = glm::vec4(xAxis, 0.0f);
						camMatrix[1] = glm::vec4(yAxis, 0.0f);
						camMatrix[2] = glm::vec4(zAxis, 0.0f);
						camMatrix[3] = glm::vec4(0, 0, 0, 1);

						camMatrix = glm::transpose(camMatrix);

						glm::mat4 leapRotMatrix;
						int cnt = 0;
						for (int i = 0; i < 4; i++)
						{
							for (int j = 0; j < 4; j++)
							{
								leapRotMatrix[i][j] = rotArray[(i * 4) + j];
							}
						}

						glm::mat4 transToOrigin(0.0f);
						transToOrigin[0].w = -zAxis[0];
						transToOrigin[1].w = -zAxis[1];
						transToOrigin[2].w = -zAxis[2];
						transToOrigin[3].w = 1;

						camMatrix = camMatrix * leapRotMatrix;

						cPos[0] = camMatrix[0].z;
						cPos[1] = camMatrix[1].z;
						cPos[2] = camMatrix[2].z;

						flag = 0;
						rotationMatrix = rotationMatrix.identity();
						glutPostRedisplay();
					}

					else
					{
						count = 0;
						flag = 0;
					}
				}
				else
				{
					//cout << "hi12" << endl;
					//cout << testFrame.hands().count() << endl;
					if (flag == 0)
					{
						//lastPosX_right = handsInFrame[0].palmPosition().x;
						//lastPosX_left =  handsInFrame[1].palmPosition().x;
						//lastPosY_right = handsInFrame[0].palmPosition().y;
						//lastPosY_left = handsInFrame[1].palmPosition().y;
						//lastPosZ_right = handsInFrame[0].palmPosition().z;
						//lastPosZ_left = handsInFrame[1].palmPosition().z;
						previousFrame = testFrame;
						grabstrength = 0.0f;
						flag = 1;
					}
					else if (count < 5)
					{
						grabstrength += handsInFrame.rightmost().grabStrength();
						grabstrength += handsInFrame.leftmost().grabStrength();
						count++;
					}
					else if (grabstrength / 5 > 0.2)
					{
						count = 0;
							if (handsInFrame[0].isLeft() == true)
							{
								Leap::Hand leftHand = handsInFrame.leftmost();
								rotationMatrix = leftHand.rotationMatrix(previousFrame);
								rotArray[16] = { 0.0f };
								rotationMatrix.toArray4x4(rotArray);
							}
							else if (handsInFrame[0].isRight() == true)
							{
								Leap::Hand rightHand = handsInFrame.rightmost();
								rotationMatrix = rightHand.rotationMatrix(previousFrame);
								rotArray[16] = { 0.0f };
								rotationMatrix.toArray4x4(rotArray);
							}

							//cout << "hi2";
						glm::vec3 zAxis(cPos[0], cPos[1], cPos[2]);
						glm::vec3 xAxis = glm::cross(glm::normalize(glm::vec3(cUpVec[0], cUpVec[1], cUpVec[2])), glm::normalize(zAxis));
						glm::vec3 yAxis = glm::cross(zAxis, xAxis);

						glm::mat4 camMatrix;
						camMatrix[0] = glm::vec4(xAxis, 0.0f);
						camMatrix[1] = glm::vec4(yAxis, 0.0f);
						camMatrix[2] = glm::vec4(zAxis, 0.0f);
						camMatrix[3] = glm::vec4(0, 0, 0, 1);

						camMatrix = glm::transpose(camMatrix);

						glm::mat4 leapRotMatrix;
						int cnt = 0;
						for (int i = 0; i < 4; i++)
						{
							for (int j = 0; j < 4; j++)
							{
								leapRotMatrix[i][j] = rotArray[(i * 4) + j];
							}
						}

						glm::mat4 transToOrigin(0.0f);
						transToOrigin[0].w = -zAxis[0];
						transToOrigin[1].w = -zAxis[1];
						transToOrigin[2].w = -zAxis[2];
						transToOrigin[3].w = 1;

						camMatrix = camMatrix * leapRotMatrix;

						cPos[0] = camMatrix[0].z;
						cPos[1] = camMatrix[1].z;
						cPos[2] = camMatrix[2].z;

						flag = 0;
						rotationMatrix = rotationMatrix.identity();
						glutPostRedisplay();
					}

					else
					{
						count = 0;
						flag = 0;

					}
				}
			}
			else if ((handsInFrame.leftmost().grabStrength() != 1) && (handsInFrame.rightmost().grabStrength() == 1))
			{
				//cout << "hi13" << endl;
				if (flag == 0)
				{
					//lastPosX_left = handsInFrame[1].palmPosition().x;
					//lastPosY_left = handsInFrame[1].palmPosition().y;
					//lastPosZ_left = handsInFrame[1].palmPosition().z;
					previousFrame = testFrame;
					grabstrength = 0.0f;
					flag = 1;
				}
				else if (count < 5)
				{
					grabstrength += handsInFrame.leftmost().grabStrength();
					count++;
				}
				else if (grabstrength / 5 > 0.1)
				{
					count = 0;
					//cout << "hi3";
					Leap::Hand leftHand = handsInFrame.leftmost();
					Leap::Matrix rotationMatrix = leftHand.rotationMatrix(previousFrame);
					 rotArray[16] = { 0.0f };
					rotationMatrix.toArray4x4(rotArray);

					glm::vec3 zAxis(cPos[0], cPos[1], cPos[2]);
					glm::vec3 xAxis = glm::cross(glm::normalize(glm::vec3(cUpVec[0], cUpVec[1], cUpVec[2])), glm::normalize(zAxis));
					glm::vec3 yAxis = glm::cross(zAxis, xAxis);

					glm::mat4 camMatrix;
					camMatrix[0] = glm::vec4(xAxis, 0.0f);
					camMatrix[1] = glm::vec4(yAxis, 0.0f);
					camMatrix[2] = glm::vec4(zAxis, 0.0f);
					camMatrix[3] = glm::vec4(0, 0, 0, 1);

					camMatrix = glm::transpose(camMatrix);

					glm::mat4 leapRotMatrix;
					int cnt = 0;
					for (int i = 0; i < 4; i++)
					{
						for (int j = 0; j < 4; j++)
						{
							leapRotMatrix[i][j] = rotArray[(i * 4) + j];
						}
					}

					glm::mat4 transToOrigin(0.0f);
					transToOrigin[0].w = -zAxis[0];
					transToOrigin[1].w = -zAxis[1];
					transToOrigin[2].w = -zAxis[2];
					transToOrigin[3].w = 1;

					camMatrix = camMatrix * leapRotMatrix;

					cPos[0] = camMatrix[0].z;
					cPos[1] = camMatrix[1].z;
					cPos[2] = camMatrix[2].z;

					flag = 0;
					rotationMatrix = rotationMatrix.identity();
					glutPostRedisplay();
				}

				else
				{
					count = 0;
					flag = 0;
				}
			}
			else if ((handsInFrame.leftmost().grabStrength() == 1) && (handsInFrame.rightmost().grabStrength() == 1))
			{

				flag = 0;
				count = 0;
				grabstrength = 0.0f;
				//cout << "hi4";
			}
			else
			{
				;
			}
	
		}
		//else if (((handsInFrame.leftmost().isValid()) == false) && (handsInFrame.rightmost().isValid()) == true)
		else if (testFrame.hands().count() == 1)
		{
			if ((handsInFrame.rightmost().grabStrength()) != 1)
			{
				//cout << "hi14" << endl;
				//cout << testFrame.hands().count() << endl;
				if (flag == 0)
				{
					lastPosX_right = handsInFrame[0].palmPosition().x;
					lastPosY_right = handsInFrame[0].palmPosition().y;
					lastPosZ_right = handsInFrame[0].palmPosition().z;
					previousFrame = testFrame;
					grabstrength = 0.0f;
					flag = 1;
				}
				else if (count < 5)
				{
					grabstrength += handsInFrame.rightmost().grabStrength();
					count++;
				}
				else if (handsInFrame.rightmost().isValid() && grabstrength / 5 > 0.1)
				{
					count = 0;
					flag = 0;
				}

				else
				{
					count = 0;
					flag = 0;
					//cout << "hi5";
					x = handsInFrame[0].palmPosition().x;
					y = handsInFrame[0].palmPosition().y;
					z = handsInFrame[0].palmPosition().z;
					flag = 0;
					mouseMotion((x - lastPosX_right) * 3, (lastPosY_right - y) * 3, z - lastPosZ_right);
					x = 0;
					y = 0;
					z = 0;
					lastPosX_right = 0;
					lastPosY_right = 0;
					lastPosZ_right = 0;

					glutPostRedisplay();
				}
			}
			else
			{
				;
			}

		}
	}
	else
	{
		flag = 0;
		count = 0;
		grabstrength = 0.0f;
	}
}
SampleListener listener;
 
float* normalize(float* vector)
{
	float length = sqrtf(powf(*vector, 2) + powf(*(vector + 1), 2) + powf(*(vector + 2), 2));

	float normalized[3];

	normalized[0] = *vector / length;
	normalized[1] = *(vector + 1) / length;
	normalized[2] = *(vector + 2) / length;

	return normalized;
}

float* crossProduct(float* vector1, float* vector2)
{
	float cpVal[3];

	cpVal[0] = (((*(vector1 + 1)) * (*(vector2 + 2))) - (*(vector1 + 2)) * (*(vector2 + 1)));
	cpVal[1] = -(((*(vector1)) * (*(vector2 + 2))) - (*(vector1 + 2)) * (*(vector2)));
	cpVal[2] = (((*(vector1)) * (*(vector2 + 1))) - (*(vector1 + 1)) * (*(vector2)));

	return cpVal;
}

void extractNormal(const string& normal, Vertex& vertInstance)
{
	string normalExtracted;

	size_t infoLoc = normal.find_first_of('(');
	size_t lastLoc = normal.find_first_of(')');

	for (size_t i = infoLoc + 1; i < lastLoc; i++)
	{
		normalExtracted.push_back(normal[i]);
	}

	stringstream buffer(normalExtracted);

	buffer >> vertInstance.normal_x;
	buffer >> vertInstance.normal_y;
	buffer >> vertInstance.normal_z;
}

void readMFile(const string& filePath)
{
	fstream fileReader(filePath);

	if (fileReader.is_open())
	{
		string strLine = "";
		while (getline(fileReader, strLine))
		{
			stringstream buffer(strLine);

			string token;

			buffer >> token;

			if (token.compare("Vertex") == 0)
			{
				Vertex vertInstance;

				buffer >> vertInstance.vert_id;

				buffer >> vertInstance.xValue;

				buffer >> vertInstance.yValue;
				buffer >> vertInstance.zValue;

				extractNormal(strLine, vertInstance);

				Vertex_t.push_back(vertInstance);
			}
			else if (token.compare("Face") == 0)
			{
				Face faceInstance;

				buffer >> faceInstance.faceID;

				int vertID;

				buffer >> vertID;
				faceInstance.vertexList.push_back(vertID);
				buffer >> vertID;
				faceInstance.vertexList.push_back(vertID);
				buffer >> vertID;
				faceInstance.vertexList.push_back(vertID);

				Face_t.push_back(faceInstance);
			}
		}
	}

	isFileDone = true;

	findScaleFactor();
}

void findScaleFactor()
{
	auto xMax = max_element(Vertex_t.begin(), Vertex_t.end(), [](Vertex vert_id1, Vertex vert_id2){
		return (vert_id1.xValue < vert_id2.xValue);
	});

	const float xm = xMax->xValue;

	auto yMax = max_element(Vertex_t.begin(), Vertex_t.end(), [](Vertex vert_id1, Vertex vert_id2){
		return (vert_id1.yValue < vert_id2.yValue);
	});

	const float ym = yMax->yValue;

	auto zMax = max_element(Vertex_t.begin(), Vertex_t.end(), [](Vertex vert_id1, Vertex vert_id2){
		return (vert_id1.zValue < vert_id2.zValue);
	});

	const float zm = zMax->zValue;

	if (xm > ym && xm > zm)
		scaleFactor = xm;
	else if (ym > xm && ym > zm)
		scaleFactor = ym;
	else
		scaleFactor = zm;
}


void mouseMotion(float x, float y, float z)
{	
	float deltaX = x;
	float deltaY = y;
	float deltaZ = z;

	double scale, length;
	float cFar[3];
	float cIdent[3];
	float cRes[3];
	
	if((abs(deltaX) > abs(deltaY) && abs(deltaX) > abs(deltaZ)) || (abs(deltaY) > abs(deltaX) && abs(deltaY) > abs(deltaZ)))
	{
		Button_index = 1;
	}
	else
	{
		Button_index = 2;
	}

	switch (Button_index)
	{
	case GLUT_MIDDLE_BUTTON:
		{
			cFar[0] = cLookAt[0] - cPos[0];
			cFar[1] = cLookAt[1] - cPos[1];
			cFar[2] = cLookAt[2] - cPos[2];
			cIdent[0] = 0.0f;
			cIdent[1] = 1.0f;
			cIdent[2] = 0.0f;

			float length = sqrt((cFar[0] * cFar[0]) + (cFar[1] * cFar[1]) + (cFar[2] * cFar[2]));

			scale = sqrt(length) * 0.003;

			cRes[0] = *(crossProduct(cFar, cIdent));
			cRes[1] = *((crossProduct(cFar, cIdent)) + 1);
			cRes[2] = *((crossProduct(cFar, cIdent)) + 2);
			cIdent[0] = *(crossProduct(cRes, cFar));
			cIdent[1] = *(crossProduct(cRes, cFar) + 1);
			cIdent[2] = *(crossProduct(cRes, cFar) + 2);

			double mag = sqrt((cRes[0] * cRes[0]) + (cRes[1] * cRes[1]) + (cRes[2] * cRes[2]));
			cRes[0] = cRes[0] / mag;
			cRes[1] = cRes[1] / mag;
			cRes[2] = cRes[2] / mag;

			mag = sqrt((cIdent[0] * cIdent[0]) + (cIdent[1] * cIdent[1]) + (cIdent[2] * cIdent[2]));
			cIdent[0] = cIdent[0] / mag;
			cIdent[1] = cIdent[1] / mag;
			cIdent[2] = cIdent[2] / mag;

			cPos[0] += -cRes[0] * deltaX * scale + cIdent[0] * deltaY * scale;
			cPos[1] += -cRes[1] * deltaX * scale + cIdent[1] * deltaY * scale;
			cPos[2] += -cRes[2] * deltaX * scale + cIdent[2] * deltaY * scale;

			cLookAt[0] += -cRes[0] * deltaX * scale + cIdent[0] * deltaY * scale;
			cLookAt[1] += -cRes[1] * deltaX * scale + cIdent[1] * deltaY * scale;
			cLookAt[2] += -cRes[2] * deltaX * scale + cIdent[2] * deltaY * scale;

			break;
		}
	case GLUT_RIGHT_BUTTON:
		{
			cFar[0] = cLookAt[0] - cPos[0];
			cFar[1] = cLookAt[1] - cPos[1];
			cFar[2] = cLookAt[2] - cPos[2];

			length = sqrt((cFar[0] * cFar[0]) + (cFar[1] * cFar[1]) + (cFar[2] * cFar[2]));
			cFar[0] = cFar[0] / length;
			cFar[1] = cFar[1] / length;
			cFar[2] = cFar[2] / length;

			length -= sqrt(length)*deltaZ*0.03;

			cPos[0] = cLookAt[0] - length * cFar[0];
			cPos[1] = cLookAt[1] - length * cFar[1];
			cPos[2] = cLookAt[2] - length * cFar[2];

			if (length < 1)
			{
				cLookAt[0] = cPos[0] + cFar[0];
				cLookAt[1] = cPos[1] + cFar[1];
				cLookAt[2] = cPos[2] + cFar[2];
			}

			break;
		}
	}

}

void Display()
{		
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	gluLookAt(cPos[0], cPos[1], cPos[2], cLookAt[0], cLookAt[1], cLookAt[2], 0, 1, 0);

	glLightfv(GL_LIGHT0, GL_POSITION, m_LightPos);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, m_AmbientLight);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular_light);
	glMateriali(GL_FRONT, GL_SHININESS, 100);

	glScalef((1 / scaleFactor), (1 / scaleFactor), (1 / scaleFactor));
	glShadeModel(GL_SMOOTH);
	glBegin(GL_TRIANGLES);
	if (isFileDone){
		for (auto face_id : Face_t)
		{
			for (auto vertex_id : face_id.vertexList)
			{
				glNormal3f(Vertex_t[vertex_id - 1].normal_x, Vertex_t[vertex_id - 1].normal_y, Vertex_t[vertex_id - 1].normal_z);
				glVertex3f(Vertex_t[vertex_id - 1].xValue, Vertex_t[vertex_id - 1].yValue, Vertex_t[vertex_id - 1].zValue);
			}
		}
	}
	glEnd();

	/****************** To draw axis ***************/
	DrawAxis();

	GLfloat bright[4] = { 2, 2, 2, 1 };
	//glPolygonOffset(1, 1);
	//glEnable(GL_POLYGON_OFFSET_FILL);
	
	/****************** To draw Grid ***************/
	glPushMatrix();
	glFrustum(-1.0, 1.0, -1.0, 1.0, 5, 100);
	DrawGrid();
	glPopMatrix();
	glFlush();
	glutSwapBuffers();

	
}	

void ReShape(int width, int height)
{
	if (height == 0) height = 1;                // To prevent divide by 0
	GLfloat aspect = (GLfloat)width / (GLfloat)height;

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width >= height) {
		//gluOrtho2D(-1.0 * aspect, 1.0 * aspect, -1.0, 1.0);
	}
	else {
		//gluOrtho2D(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect);
	}
	
	gluPerspective(50.0f, aspect, 1.0f, 100.0f);
}

void Idle()
{	
	glutPostRedisplay();   // Post a re-paint request to activate display()
}
void DrawGrid()
{
	glTranslatef(0.2f, -0.3f, 0.0f);     // Translate right and down
	//glRotatef(180.0f, 0.0f, 0.0f, 1.0f); // Rotate 180 degree
	glOrtho(-1.0, 1.0, -1.0, 1.0, 5, 100);
	//glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
glColor3f(0.25f, 0.25f, 0.25f);
for (int i = -45000; i <= 45000; i += 2000)
{
	glVertex3f((float)i, 0, (float)-45000);
	glVertex3f((float)i, 0, (float)45000);

	glVertex3f((float)-45000, 0, (float)i);
	glVertex3f((float)45000, 0, (float)i);

	glVertex3f((float)-45000, (float)i, 0);
	glVertex3f((float)45000, (float)i, 0);

	glVertex3f((float)i, (float)-45000, 0);
	glVertex3f((float)i, (float)45000, 0);

	glVertex3f((float)0, (float)-45000, i);
	glVertex3f((float)0, (float)45000, i);

	glVertex3f((float)0, i, (float)-45000);
	glVertex3f((float)0, i, (float)45000);
}
glEnd();



}

void DrawAxis()
{
	glPushMatrix();
	glLineWidth(4.5);
	glBegin(GL_LINES);
	glEnable(GL_LINE_SMOOTH);

	glColor3f(1.0f, 0.0f, 0.0f); // X axis is red.
	glVertex3f(-45000, 0, 0);
	glVertex3f(45000, 0, 0);

	glColor3f(0.0f, 1.0f, 0.0f); // Y axis is blue.
	glVertex3f(0, -45000, 0);
	glVertex3f(0, 45000, 0);

	glColor3f(0.0f, 0.0f, 1.0f); // Z axis is green.
	glVertex3f(0, 0, -45000);
	glVertex3f(0, 0, 45000);

	glEnd();
	glLineWidth(1.0);

	glPopMatrix();

}
/* Initialize OpenGL Graphics */
void initGL() {
	// Set "clearing" or background color
	glClearColor(0.0f, 1.0f, 1.0f, 1.0f); // Black and opaque
}

/* Callback handler for normal-key event */
void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 27:     // ESC key
		exit(0);
		break;
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);          // Initialize GLUT
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(1200, 800);

	glutCreateWindow("MeshViewer_Project_HCI");
	glutInitWindowPosition(50, 50); // Position the window's initial top-left corner
	initGL();                       // Our own OpenGL initialization
	glEnable(GL_DEPTH_TEST);
	
	glutDisplayFunc(Display);
	glutReshapeFunc(ReShape);
	glutIdleFunc(Idle);
	glutKeyboardFunc(keyboard);     // Register callback handler for special-key event
	//glutFullScreen();             // Put into full screen
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);
	//readMFile("bunny.m");
	readMFile("gargoyle.m");
	controller.addListener(listener);

	glutMainLoop();
	return 0;
}
